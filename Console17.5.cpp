﻿#include <iostream>
#include <cmath>

using namespace std;

class Exemple
{
private:
	int a;
public:
	int GetA()
	{
		return a;
	}

	void SetA(int newA)
	{
		a = newA;
	}
};

	class Vector

	{
	public:

		Vector() : x(0), y(0), z(0)
		{}
		Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
		{}

		double getLength(const double _x, const double _y, const double _z)
		{
			return hypot(_x, hypot(_y, _z));
		}

		void Show()

		{
			cout << "    The module of the Vector = " << getLength(1, 2, 3) << '\n';
		}


	private:
		double x;
		double y;
		double z;
	};

	int main()
	{
		Exemple temp, temp1;
		temp.SetA(12345);
		temp1.SetA(54321);
		std::cout << " Data output " << temp.GetA() << ' ' << temp1.GetA();
		
		Vector v(1, 2, 3);
		v.Show();
	}
